#!/usr/bin/env python
# -*- coding: utf-8 -*-

from module.ebs_controller import *
from module.ec2_controller import *
from module.cloud_sys_scan import *
from module.cloud_baseline_scan import *
from module.reclaim_controller import reclaim_core
from module.func.common import *


def core_func():
    """
    扫描控制核心函数，用于调用各个子模块扫描功能
    :return:
    """
    # ==================================加载配置文件========================================
    load_auth_config()
    # ===============================收集云环境资源信息=====================================
    print "*" * 40, "云环境资产信息收集", "*" * 40
    get_resource_map()
    # ===========================对云环境中实例进行系统扫描=================================
    print "*" * 40, "开始云服务器扫描流程", "*" * 40
    key_pair = str(read_config("SSH-Key-Pair", "key-name"))
    create_key_pair(keyname=key_pair)
    num, instance_info, instance_id = get_instance_info()
    bak_instance_info = bak_instance(instance_id, key_pair)
    for bak_id in bak_instance_info:
        public_ip = str(bak_instance_info[bak_id]["public_ip"]).strip()
        scan_control(public_ip, key_pair)
    # ============================对扫描所创建资源进行回收==================================
    print "*" * 40, "开始回收临时创建资源", "*" * 40
    reclaim_core(bak_instance_info, key_pair)
    # ============================对云环境安全基线进行扫描==================================
    print "*" * 40, "开始云基线扫描流程", "*" * 40
    cloud_scan_server()


def setup_basline_tool():
    """
    安装扫描工具
    :return:
    """
    path = os.path.abspath(os.path.dirname(__file__)) + "/module/cloudsploit/"
    try:
        subprocess.check_call("npm install", cwd=path, shell=True)
        print "[+]Cloud baseline check tools install success.[+]"
        return 1
    except:
        print "[+]Cloud baseline check tools install error.[+]"
        return 0


def bak_instance(instance_list, key_pair):
    """
    备份系统EC2实例，用于扫描使用
    :param instance_list:
    :param key_pair:
    :return:
    """
    import time
    bak_instance_info_dict = dict()
    for instance_id in instance_list:
        # 获取实例的基本信息，为后续克隆新实例提供必要信息
        print u"[+]Get instance info[+]"
        instance_info = get_single_instance_info(instance_id)
        # 通过实例创建image镜像，为克隆新实例提供镜像源
        print u"[+]Create image for instance %s,that's will take some minutes,please wait......[+]" % instance_id
        image_ids = create_image(instance_id)["ImageId"]
        time.sleep(150)
        # 通过获取到的信息创建克隆实例
        print u"[+]Create copy one for instance %s.[+]" % instance_id
        try:
            new_instance_id = create_instance(image_id=image_ids,
                                              security_group_id=instance_info["security_group_id"],
                                              subnet_id=instance_info["subnet_id"],
                                              instance_type=instance_info["instance_type"],
                                              key_name=key_pair)
        except:
            print u"[+]Image is pending, scanner try to recreate copy instance,please wait......[+]"
            time.sleep(100)
            new_instance_id = create_instance(image_id=image_ids,
                                              security_group_id=instance_info["security_group_id"],
                                              subnet_id=instance_info["subnet_id"],
                                              instance_type=instance_info["instance_type"],
                                              key_name=key_pair)
        time.sleep(2)
        # 获取克隆实例的基本信息，主要为获取其IP地址便于后续登录扫描
        new_instance_info = get_single_instance_info(new_instance_id)
        bak_instance_info_dict[new_instance_id] = new_instance_info
    return bak_instance_info_dict


def get_resource_map():
    """
    收集云端所有资源
    :return:
    """
    resource_dict = dict()
    # 获取当前所有的实例
    resource_dict['instance'] = exec_aws_command(["ec2", "describe-instances", "--query",
                                                  "Reservations[*].Instances[*].{Instance:InstanceId,praviteip:PrivateIpAddress,publicip:PublicIpAddress}"])
    # 获取当前的所有快照
    resource_dict['snapshots'] = exec_aws_command(
        ["ec2", "describe-snapshots", "--owner-ids", "self", "--query", "Snapshots[*].SnapshotId"])
    # 获取当前的所有镜像
    resource_dict['images'] = exec_aws_command(
        ["ec2", "describe-images", "--owners", "self", "--query", "Images[*].ImageId"])
    # 获取当前所有的卷
    resource_dict['volumes'] = exec_aws_command(["ec2", "describe-volumes", "--query", "Volumes[*].VolumeId"])
    # 获取当前所有的buckets
    resource_dict['buckets'] = exec_aws_command(["s3", "ls"]).strip()
    # 获取cloudtrail信息
    resource_dict['cloudtrail'] = exec_aws_command(["cloudtrail", "describe-trails", "--query", "trailList[*].Name"])
    data_time = str(time.strftime('%Y-%m-%d ', time.localtime(time.time())).strip() + "_" + time.strftime('%H-%M-%S',
                                                                                                          time.localtime(
                                                                                                              time.time())))
    local_dir = base_dir + "/output/%s__cloud_resource.map" % (data_time)
    obj = open(local_dir, "w+")
    for key in resource_dict:
        obj.writelines(key + ":" + str(resource_dict[key]) + "\n")
    print "ResourceMap has been saved in %s" % local_dir


# core_func()
# get_resource_map()
# connect_server(host="3.136.19.29",keyname="tanzhen.pem")
# scan_control(host="3.22.249.107", keyname="tanzhen")
