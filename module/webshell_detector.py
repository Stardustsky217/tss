#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import os
base_dir = os.path.dirname(__file__)


def webshell_scan(ssh,sftp,host,logger):
    logger.info("[+]Start webshell scanning[+]")
    path = "/var/www/"
    stdin, stdout, stderr = ssh.exec_command("sudo tar -zvxf /tmp/sec/webshell-detector.tar.gz -C /tmp/sec/")
    stdin, stdout, stderr = ssh.exec_command("sudo chmod 777 /tmp/sec/webshell-detector")
    exec_order = "sudo /tmp/sec/webshell-detector -html -output /tmp/sec/report/webshell-check.html %s" % path
    stdin, stdout, stderr = ssh.exec_command(exec_order)
    stdout.read()
    download_report(sftp=sftp,host=host,logger=logger)


def download_report(sftp,host,logger):
    data_time = str(time.strftime('%Y-%m-%d ', time.localtime(time.time())).strip() + "_" + time.strftime('%H-%M-%S',
                                                                                                          time.localtime(
                                                                                                              time.time())))
    local_dir = base_dir + "/output/%s/%s_wenshell_scan.html" % (host, data_time)
    tool_list = sftp.get("/tmp/sec/report/webshell-check.html", local_dir)
    logger.info("[+]Webshell scan report download success and saved in %s.[+]" % local_dir)
    return tool_list