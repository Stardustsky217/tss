#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cli import *

def get_snapshot_info():
    """
    获取snapshot信息
    :return:
    """


def create_snapshot(volume_id):
    """
    创建snapshot
    :return:
    """
    snapshot = exec_aws_command(["ec2", "create-snapshot", "--description", "machine-test", "--volume-id", volume_id])
    return snapshot


def delete_snapshot(snapshot_id):
    """
    删除snapshot镜像
    :return:
    """
    res = exec_aws_command(["ec2", " delete-snapshot", "--snapshot-id", snapshot_id])
    return res

def create_volume(client):
    """
    创建volume卷
    :return:
    """
    response = exec_aws_command(["create-volume", "--availability-zone", "us-east-2"])


def delete_volume(volume_id):
    """
    删除volume卷
    :return:
    """
    res = exec_aws_command(["ec2", " delete-volume", "--volume-id", volume_id])
    return res

def mount_volume():
    """
    挂载volume卷
    :return:
    """


def unmount_volume(volume_id):
    """
    解除volume挂载
    :return:
    """
    res = exec_aws_command(["ec2", " detach-volume", "--volume-id", volume_id])
    return res