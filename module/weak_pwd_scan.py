#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import paramiko
import subprocess
import shutil
import time

base_dir = os.path.dirname(__file__)


def trans_connect(host, keyname, username="ubuntu"):
    private_key = paramiko.RSAKey.from_private_key_file(base_dir + '/config/' + keyname + ".pem")
    transport = paramiko.Transport((host, 22))
    transport.connect(username=username, pkey=private_key)
    sftp = paramiko.SFTPClient.from_transport(transport)
    return sftp


def local_pwd_scan(ssh, sftp, host):
    """
    本地密码破解函数
    :param ssh:
    :param host:
    :param keyname:
    :return:
    """
    data_time = str(time.strftime('%Y-%m-%d ', time.localtime(time.time())).strip() + "_" + time.strftime('%H-%M-%S',
                                                                                                          time.localtime(
                                                                                                              time.time())))
    try:
        print "[+]Start cracking local password.[+]"
        ssh.exec_command("sudo cp /etc/shadow /tmp/shadow")
        ssh.exec_command("sudo chmod 777 /tmp/shadow")
        sftp.get("/tmp/shadow", base_dir+"/tmp/shadow")
        pwd_file = base_dir + "/tmp/shadow"
        pwd_crack_file = base_dir + "/tools/john/john.pot"
        out_file = base_dir + "/output/%s/%s_pwd_crack.log" % (host, data_time)
        exec_list = ["john", "--wordlist=password.lst", pwd_file]
        p = subprocess.Popen(exec_list, cwd=base_dir+"/tools/john/", shell=True)
        p.communicate()[0]
        shutil.copyfile(pwd_crack_file, out_file)
        obj = open(pwd_crack_file, "w")
        obj.write("")
    except:
        print "[+]Error found in crack process,please confirm the path.[+]"
    else:
        print "[+]Crack finish and saved in %s.[+]" % out_file


def reomote_pwd_scan(host):
    """
    该模块不太适合集成到产品之中，适合独立使用
    :param host:
    :return:
    """
    service_dict = {"ssh":22, "rdp":3389,"ftp":21,"mysql":3306,"telnet":23}
    cwd_path = base_dir + "/tools/hydra/"
    try:
        print "[+]Start scanning protocol weak password.[+]"
        for serv in service_dict:
            if serv == "ssh":
                print "-- ssh weak password check --"
                exec_list = ["hydra", "-L", "dict/ssh-user.txt", "-P", "dict/ssh-pass.txt", "-t", "16","-K","-V", "ssh://%s" % host]
            if serv == "rdp":
                print "-- windows rdp weak password check --"
                exec_list = ["hydra", "-L", "dict/rdp-user.txt", "-P", "dict/rdp-pass.txt", "-t", "16","-K", "-V","rdp://%s" % host]
            if serv == "ftp":
                print "-- ftp weak password check --"
                exec_list = ["hydra", "-L", "dict/ftp-user.txt", "-P", "dict/ftp-pass.txt", "-t", "16","-K", "-V","ftp://%s" % host]
            if serv == "mysql":
                print "-- mysql weak password check --"
                exec_list = ["hydra", "-L", "dict/mysql-user.txt", "-P", "dict/mysql-pass.txt", "-t", "16","-K", "-V","mysql://%s" % host]
            if serv == "telnet":
                print "-- telnet weak password check --"
                exec_list = ["hydra", "-L", "dict/telnet-user.txt", "-P", "dict/telnet-pass.txt", "-t", "16","-K", "-V","telnet://%s" % host]
            p = subprocess.Popen(exec_list, cwd=cwd_path, shell=True, close_fds=True)
            p.communicate()[0]
    except:
        print "[+]Error found in crack process,please confirm the path.[+]"


def crack_center(ssh,host,keyname):
    local_pwd_scan(ssh, host, keyname)
    reomote_pwd_scan(host)


# ssh = connect_server("52.14.56.166", "test")
# local_pwd_scan(ssh,"52.14.56.166", "test")
# reomote_pwd_scan("52.14.56.166")

# host= "52.14.56.166"
# cwd_path = base_dir + "/tools/hydra/"
# exec_list = ["hydra", "-L", "dict/mysql-user.txt", "-P", "dict/mysql-pass.txt", "-t", "16","-V","-K","-q","mysql://%s" % host]
# p = subprocess.Popen(exec_list, cwd=cwd_path, shell=True)
# p.communicate()[0]



