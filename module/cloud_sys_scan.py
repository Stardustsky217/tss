#!/usr/bin/env python
# -*- coding: utf-8 -*-

from weak_pwd_scan import *
from virus_detector import virus_scan
from webshell_detector import webshell_scan
from leakinfo_detector import leakinfo_scan
from func.common import init_logger
import paramiko
import glob
import time
import os

base_dir = os.path.dirname(__file__)
logger = init_logger()


def connect_server(host, keyname="", port=22, username="ubuntu"):
    """
    连接远程服务器
    :param host:
    :param keyname:
    :param port:
    :param username:
    :return:
    """
    private_key = paramiko.RSAKey.from_private_key_file(base_dir + '/config/' + keyname + ".pem")
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        time.sleep(10)
        ssh.connect(hostname=host, port=port, username=username, pkey=private_key)
        logger.info("[+]Connect %s success.[+]" % host)
        return ssh
    except:
        logger.warning("[+]Connect %s failed, try to reconnect server.[+]" % host)
        try:
            time.sleep(20)
            ssh.connect(hostname=host, port=port, username=username, pkey=private_key)
            logger.info("[+]Connect %s success.[+]" % host)
            return ssh
        except:
            logger.warning("[+]Connect %s failed, please confirm the key pair or the instance status.[+]" % host)
            exit(0)


def exec_command(ssh, command):
    """
    执行远程系统命令
    :param ssh:
    :param command:
    :return:
    """
    stdin, stdout, stderr = ssh.exec_command(command)
    result = stdout.read()
    return result.decode()


def sftp_server(host, keyname, port=22, username="ubuntu"):
    private_key = paramiko.RSAKey.from_private_key_file(base_dir + '/config/' + keyname + ".pem")
    transport = paramiko.Transport((host, port))
    transport.connect(username=username, pkey=private_key)
    sftp = paramiko.SFTPClient.from_transport(transport)
    return sftp


def upload_scan_tool(sftp,remote_dir="/tmp/"):
    files = glob.glob(base_dir + "/tools/*.gz")
    sftp.mkdir(remote_dir + "/sec/")
    sftp.mkdir(remote_dir + "/sec/report/")
    tool_list = []
    for i in files:
        des_file = remote_dir + "sec/" + i.split("\\")[-1]
        sftp.put(i, des_file)
        tool_list.append(des_file)
    logger.info("[+]Scanner upload success.[+]")
    return tool_list


def create_report_dir(host):
    report_path = base_dir + "/output/%s/" % host
    is_exists = os.path.exists(report_path)
    if not is_exists:
        os.mkdir(base_dir + "/output/%s/" % host)
    else:
        pass


def scan_control(host, keyname):
    """
    远程扫描总控制
    :return:
    """
    user = "ubuntu"
    ssh = connect_server(host=host, keyname=keyname,username=user)
    sftp = sftp_server(host=host,keyname=keyname,username=user)
    try:
        # 上传漏洞扫描文件
        upload_scan_tool(sftp)
    except:
        logger.warning("/tmp/sec file exist,try to delete it and recreate!!!")
        exec_command(ssh, "sudo rm -rf /tmp/sec/")
        upload_scan_tool(sftp)
    try:
        logger.info("[+]Start scanning system.[+]")
        # ===================创建报告存放目录=======================
        create_report_dir(host=host)
        # ================lynis系统漏洞及病毒扫描====================
        virus_scan(ssh=ssh, sftp=sftp, host=host, logger=logger)
        # ====================Webshell扫描检查=======================
        webshell_scan(ssh=ssh, sftp=sftp, host=host, logger=logger)
        # ====================Leakinfo扫描检查=======================
        leakinfo_scan(ssh=ssh, sftp=sftp, host=host, logger=logger)
        # ====================系统弱口令破解=========================
        local_pwd_scan(ssh=ssh, sftp=sftp, host=host)
        print "[+]Scan system finish.[+]"
    except:
        logger.warning("Unpack or execute scanner failed!!!")



# scan_control(host="3.22.249.107", keyname="tanzhen")
scan_control(host="13.59.227.52", keyname="sai")
# connect_server(host="13.58.201.173",keyname="tanzhen")