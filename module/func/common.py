#!/usr/bin/env python
# -*- coding: utf-8 -*-
import configparser
import os
import logging


def read_config(option, key):
    base_dir = os.path.dirname(__file__)
    config = configparser.ConfigParser()
    config.read(base_dir+"/../config/config.ini")
    value = config.get(option, key)
    return value.strip()


def init_logger():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s -  %(levelname)s - %(message)s')
    logger = logging.getLogger(__name__)
    return logger
