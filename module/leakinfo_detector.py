#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import os

base_dir = os.path.dirname(__file__)


def leakinfo_scan(ssh, sftp, host, logger):
    logger.info("[+]Start leakinfo scanning[+]")
    stdin, stdout, stderr = ssh.exec_command("sudo tar -zvxf /tmp/sec/leakinfo_detector.tar.gz -C /tmp/sec/")
    stdin, stdout, stderr = ssh.exec_command("sudo python3 /tmp/sec/leakinfo_detector.py")
    stdout.read()
    download_report(sftp=sftp, host=host, logger=logger)


def download_report(sftp, host, logger):
    data_time = str(time.strftime('%Y-%m-%d ', time.localtime(time.time())).strip() + "_" + time.strftime('%H-%M-%S',
                                                                                                          time.localtime(
                                                                                                              time.time())))
    local_dir = base_dir + "/output/%s/%s_leakinfo_scan.log" % (host, data_time)
    tool_list = sftp.get("/tmp/sec/report/leak_info.log", local_dir)
    logger.info("[+]Leak info scan report download success and saved in %s.[+]" % local_dir)
    return tool_list
