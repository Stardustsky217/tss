#!/usr/bin/env python
# -*- coding: utf-8 -*-
from cli import *
import configparser

base_dir = os.path.dirname(__file__)


def get_instance_info():
    """
    获取所有实例各项基本信息
    :return:
    """
    print "[+]Starting collect instance infomation...[+]"
    instance = exec_aws_command(["ec2", "describe-instances"])
    instance_group = dict()
    instance_id = list()
    for i in instance["Reservations"]:
        index = i["Instances"][0]["InstanceId"]
        if i["Instances"][0]["State"]["Name"] == "running":
            instance_group[index] = dict()
            instance_group[index]["public_ip"] = i["Instances"][0]["PublicIpAddress"]
            instance_group[index]["private_ip"] = i["Instances"][0]["PrivateIpAddress"]
            instance_group[index]["vpc_id"] = i["Instances"][0]["VpcId"]
            instance_group[index]["instance_type"] = i["Instances"][0]["InstanceType"]
            instance_group[index]["security_group_id"] = i["Instances"][0]["SecurityGroups"][0]["GroupId"]
            instance_group[index]["subnet_id"] = i["Instances"][0]["SubnetId"]
            instance_group[index]["image_id"] = i["Instances"][0]["ImageId"]
            instance_group[index]["network_interfaces"] = i["Instances"][0]["NetworkInterfaces"]
            instance_group[index]["instance_id"] = i["Instances"][0]["InstanceId"]
            instance_group[index]["volume_id"] = i["Instances"][0]["BlockDeviceMappings"][0]["Ebs"]["VolumeId"]
            instance_group[index]["key_name"] = i["Instances"][0]["KeyName"]
            instance_group[index]["owner_id"] = i["OwnerId"]
            instance_id.append(i["Instances"][0]["InstanceId"])
    instance_num = len(instance_group)
    return instance_num, instance_group, instance_id


def get_single_instance_info(instance_id):
    instance_info = dict()
    instance = exec_aws_command(["ec2", "describe-instances", "--instance-ids", instance_id])
    instance_obj = instance["Reservations"][0]
    instance_info["public_ip"] = instance_obj["Instances"][0]["PublicIpAddress"]
    instance_info["vpc_id"] = instance_obj["Instances"][0]["VpcId"]
    instance_info["subnet_id"] = instance_obj["Instances"][0]["SubnetId"]
    instance_info["instance_type"] = instance_obj["Instances"][0]["InstanceType"]
    instance_info["image_id"] = instance_obj["Instances"][0]["ImageId"]
    instance_info["instance_id"] = instance_obj["Instances"][0]["InstanceId"]
    instance_info["security_group_id"] = instance_obj["Instances"][0]["SecurityGroups"][0]["GroupId"]
    instance_info["volume_id"] = instance_obj["Instances"][0]["BlockDeviceMappings"][0]["Ebs"]["VolumeId"]
    return instance_info


def shift_instance_status(shift, instance_id):
    """
    开关实例
    :return:
    """
    if shift == "stop":
        res = exec_aws_command(["ec2", "stop-instances", "--instance-ids", instance_id, "--force"])
    if shift == "start":
        res = exec_aws_command(["ec2", "start-instances", "--instance-ids", instance_id])
    return res


def create_instance(image_id="", security_group_id="", subnet_id="", instance_type="", key_name=""):
    """
    创建instance实例
    :return:
    """
    new_instance = exec_aws_command(["ec2", "run-instances",
                  "--image-id", image_id,
                  "--security-group-ids", security_group_id,
                  "--count", "1",
                  "--subnet-id", subnet_id,
                  "--key-name", key_name,
                  "--instance-type", instance_type,
                  "--associate-public-ip-address"
                  ])
    # new_instance = exec_command(["ec2", "run-instances",
    #                               "--image-id", "ami-0f6be09204a85bf14",
    #                               "--security-group-ids", "sg-04948a6e7a060cd0c",
    #                               "--count", "1",
    #                               "--subnet-id", "subnet-03ef043e7beebbff1",
    #                               "--key-name", "security_key",
    #                               "--instance-type", "t2.micro",
    #                               "--associate-public-ip-address"
    #               ])
    # print new_instance
    new_instance_id = new_instance["Instances"][0]["InstanceId"]
    return new_instance_id


def modify_instance_attribute(instance_id):
    exec_aws_command(["ec2", "--instance-id", instance_id])


def delete_instance():
    """
    删除instance实例
    :return:
    """


def delete_image():
    pass


def create_key_pair(keyname):

    try:
        res = exec_aws_command(["ec2", "create-key-pair", "--key-name", keyname, "--query", "KeyMaterial", "--output", "text"])
        if res:
            obj = open(base_dir + "/config/" + keyname + ".pem", "w+")
            res = [item.replace("\r", "") for item in res]
            obj.writelines(res)
            print "[+]Create key pair success[+]"
    except:
        print "[+]Create key pair failed,please check the duplicate key name or aws_secret_key.[+]"
    return keyname


def create_image(instance_id):
    image_id = exec_aws_command(
        ["ec2", "create-image", "--instance-id", instance_id, "--name", "%s-SideScanServer" % instance_id, "--description",
         "image for security scan", "--no-reboot"])
    if image_id != "":
        return image_id
    else:
        print "[+]Create image failed, please check the duplicate image name.[+]"


def remove_all_bak_resource(resource_list):
    pass

# print get_instance_info()
# print shift_instance_status("start","i-0783313c44aebfef0")
# print create_key_pair("tanzhen6")
# create_instance()
# print create_image("i-0783313c44aebfef0")
# get_single_instance_info("i-0783313c44aebfef0")
