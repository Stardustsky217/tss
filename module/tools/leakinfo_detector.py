#!/usr/bin/env python
# -*- coding: utf-8 -*-
import stat
import os
import re
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


def info_scan_control():
    """
    leak info scan control
    :return:
    """
    log_path = ["/usr/local/apache/logs/","/var/log/nginx/","/var/www/"]
    # log_path = ["testdir/"]
    log_path_list = list()
    for path in log_path:
        log_path_list = log_path_list + collect_files(path, [])
    log_path_list = filter_log(log_path_list)
    leak_info = scan_content(log_path_list)
    save_result(leak_info)


def collect_files(path, all_files):
    """
    Traverse the document
    :param path:
    :param all_files:
    :return:
    """
    try:
        file_list = os.listdir(path)
        for file in file_list:
            cur_path = os.path.join(path, file)
            if os.path.isdir(cur_path):
                collect_files(cur_path, all_files)
            else:
                all_files.append(cur_path)
    except:
        logger.warning("[+]%s is not exist.[+]" % path)
    return all_files


def is_elf_file(filepath):
    """
    elf file filter
    :param filepath:
    :return:
    """
    if not os.path.exists(filepath):
        logger.info('file path {} doesnot exits'.format(filepath))
        return False
    try:
        FileStates = os.stat(filepath)
        FileMode = FileStates[stat.ST_MODE]
        if not stat.S_ISREG(FileMode) or stat.S_ISLNK(FileMode):
            return False
        with open(filepath, 'rb') as f:
            header = (bytearray(f.read(4))[1:4]).decode(encoding="utf-8")
            # logger.info("header is {}".format(header))
            if header in ["ELF"]:
                # print header
                return True
    except UnicodeDecodeError as e:
        # logger.info("is_ELFfile UnicodeDecodeError {}".format(filepath))
        # logger.info(str(e))
        pass
    return False


def is_media_file(filepath):
    """
    media file filter
    :param filepath:
    :return:
    """
    if not os.path.exists(filepath):
        logger.info('file path {} doesnot exits'.format(filepath))
        return False
    try:
        ext = ["jpg", "gif", "png", "bmp", "tif", "mp3", "rmvb", "mp4", "avi", "3gp", "flv", "wmv", "swf"]
        file_ext = filepath.split(".")[-1]
        if file_ext in ext:
            return True
    except UnicodeDecodeError as e:
        pass
    return False


def is_office_file(filepath):
    """
    office file filter
    :param filepath:
    :return:
    """
    if not os.path.exists(filepath):
        logger.info('file path {} doesnot exits'.format(filepath))
        return False
    try:
        ext = ["doc", "docx", "ppt", "pptx", "xls", "xlsx"]
        file_ext = filepath.split(".")[-1]
        if file_ext in ext:
            return True
    except UnicodeDecodeError as e:
        pass
    return False


def filter_log(file_log_list):
    """
    filter function
    :param file_log_list:
    :return:
    """
    file_list = file_log_list
    for file in file_list:
        if is_elf_file(file):
            file_list.remove(file)
        if is_media_file(file):
            file_list.remove(file)
        if is_office_file(file):
            file_list.remove(file)
    return file_list


def scan_content(file_list):
    """
    Leak info scan core
    :param file_list:
    :return:
    """
    id_card_reg = "([1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx])|([1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2})"
    bank_card_reg = "/(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})/"
    info_leak = list()
    for file in file_list:
        file_content = open(file, "rb+").read()
        id_obj = re.search(id_card_reg, str(file_content))
        bank_obj = re.search(bank_card_reg, str(file_content))
        try:
            if id_obj.group():
                info_leak.append(file + ":" + id_obj.group())
            if bank_obj.group():
                info_leak.append(file + ":" + bank_obj.group())
        except:
            pass
    return info_leak


def save_result(content):
    path = "/tmp/sec/report/leak_info.log"
    obj = open(path, "wb+")
    obj.writelines(content)
    logger.info("[+]Leak info scan success.[+]")


if __name__ == '__main__':
    info_scan_control()

