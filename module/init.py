#!/usr/bin/env python
# -*- coding: utf-8 -*-
from cli import exec_aws_command
import os

base_dir = os.path.dirname(__file__)


def create_key_pair(keyname):
    """
    创建密钥对
    :param keyname:
    :return:
    """
    try:
        res = exec_aws_command(
            ["ec2", "create-key-pair", "--key-name", keyname, "--query", "KeyMaterial", "--output", "text"])
        if res:
            obj = open(base_dir + "/config/" + keyname + ".pem", "w+")
            res = [item.replace("\r", "") for item in res]
            obj.writelines(res)
            print "[+]Create key pair success[+]"
    except:
        print "[+]Create key pair failed,please check the duplicate key name or aws_secret_key.[+]"
    return keyname


def create_security_group(vpc_id):
    """
    创建安全组策略
    :param vpc_id:
    :return:
    """
    # aws ec2 authorize-security-group-egress --group-id sg-1a2b3c4d --ip-permissions IpProtocol=tcp,FromPort=80,
    # ToPort=80,IpRanges = '[{CidrIp=10.0.0.0/16}]'
    try:
        group_id = exec_aws_command(
            ["ec2", "create-security-group", "--group-name", "tss-group", "--vpc-id", vpc_id, "--description",
             "for tss scan"])["GroupId"]
        egress_rule = "IpProtocol=tcp,FromPort=0,ToPort=65535,IpRanges = [{CidrIp=0.0.0.0/0}]"
        exec_aws_command(
            ["ec2", "authorize-security-group-egress", "--group-id", group_id, "--ip-permission", egress_rule])
        ingress_rule = "IpProtocol=tcp,FromPort=0,ToPort=65535,IpRanges = [{CidrIp=0.0.0.0/0}]"
        exec_aws_command(
            ["ec2", "authorize-security-group-ingress", "--group-id", group_id, "--ip-permission", ingress_rule])
        return group_id
    except:
        print "some error."


def create_subnet(vpc_id):
    """
    创建子网
    :param vpc_id:
    :return:
    """
    cidr_block = "10.0.2.0/24"
    try:
        subnet = exec_aws_command(
            ["ec2", "create-subnet", "--vpc-id", vpc_id, "--cidr-block", cidr_block])
    except:
        "[+]Create subnet error.[+]"
    return subnet["Subnet"]["SubnetId"]


def create_vpc():
    """
    创建VPC
    :return:
    """
    cidr_block = "10.0.2.0/24"
    try:
        vpc_id = exec_aws_command(
            ["ec2", "create-vpc", "--cidr-block", cidr_block])
    except:
        "[+]Create vpc error.[+]"
    return vpc_id["Vpc"]["VpcId"]


def create_ami():
    """
    定义使用的ami
    :return:
    """
    ami_id = "ami-03d64741867e7bb94"
    return ami_id


def collector_count(instance_num):
    """
    collector节点计算
    :param instance_num:
    :return:
    """
    try:
        if 0 < instance_num <= 8:
            c_num = 1
        elif 8 < instance_num <= 20:
            c_num = 2
        elif 20 < instance_num:
            c_num = 3
        return c_num
    except:
        print "Instance num error."
        exit(0)


def create_collector(key_pair, security_group, subnet_id, ami_id, instance_num="1", instance_type="t2.micro"):
    """
    创建数据采集器
    :param key_pair:
    :param security_group:
    :param subnet_id:
    :param ami_id:
    :param instance_num:
    :param instance_type:
    :return:
    """
    c_num = collector_count(instance_num)
    collector_list = exec_aws_command(["ec2", "run-instances",
                                       "--image-id", ami_id,
                                       "--security-group-ids", security_group,
                                       "--count", c_num,
                                       "--subnet-id", subnet_id,
                                       "--key-name", key_pair,
                                       "--instance-type", instance_type,
                                       "--associate-public-ip-address"
                                       ])
    return collector_list


def init_main():
    init_dict = dict()
    init_dict["key_pair"] = create_key_pair("tanzhen")
    init_dict["vpc_id"] = create_vpc()
    init_dict["security_group"] = create_security_group(init_dict["vpc_id"])
    init_dict["subnet_id"] = create_subnet(init_dict["vpc_id"])
    init_dict["ami_id"] = create_ami()
    collectors = create_collector(
        key_pair=init_dict["key_pair"],
        security_group=init_dict["security_group"],
        subnet_id=init_dict["subnet_id"],
        ami_id=init_dict["ami_id"],
        instance_num="1"
    )
    return init_dict, collectors



# print create_vpc()
# print create_subnet()
