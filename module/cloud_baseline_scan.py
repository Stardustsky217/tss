#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import shutil
import time
import os


def cloud_scan_server():
    base_dir = os.path.dirname(__file__)
    output_type = "csv"
    try:
        data_time = str(time.strftime('%Y-%m-%d ', time.localtime(time.time())).strip()+"_"+time.strftime('%H-%M-%S',time.localtime(time.time())))
        file_name = data_time+"_baseline_check.csv"
        exec_list = ["node", "index.js", "--"+output_type, file_name]
        p = subprocess.Popen(exec_list, cwd=base_dir+"/cloudsploit/", shell=True)
        p.communicate()[0]
        shutil.move(base_dir+"/cloudsploit/"+file_name, base_dir+"/output/"+file_name)
    except:
        print "[+]Some error happen,may check the node environment[+]"
    else:
        print "[+]Cloud Baseline check finish.[+]"