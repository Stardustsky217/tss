#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess
import json
from func.common import read_config


def load_auth_config():
    try:
        aws_key_id = read_config("Aws-Key","aws_key_id")
        aws_secret_key = read_config("Aws-Key","aws_secret_key")
        aws_regoin = read_config("Aws-Key","aws_regoin")
        exec_aws_command(["configure", "set", "aws_access_key_id", aws_key_id])
        exec_aws_command(["configure", "set", "aws_secret_access_key", aws_secret_key])
        exec_aws_command(["configure", "set", "default.region", aws_regoin])
        print "[+]Configure success.[+]"
    except :
        print "[+] Configure error,please check your AWS key.[+]"


def exec_aws_command(command_list):
    """
    调用执行系统命令
    :param command_list:
    :return:
    """
    path = os.path.abspath(os.path.dirname(__file__))+"/AWSCLIV2/aws.exe"
    exec_list = [path]
    for cm in command_list:
        exec_list.append(cm)
    p = subprocess.Popen(exec_list, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    res = p.communicate()[0]
    try:
        res = json.loads(res)
    except:
        pass
    return res

