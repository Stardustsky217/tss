#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import os
base_dir = os.path.dirname(__file__)


def virus_scan(ssh,sftp,host,logger):
    logger.info("[+]Start virus scanning......[+]")
    stdin, stdout, stderr = ssh.exec_command("sudo tar -zvxf /tmp/sec/lynis.tar.gz -C /tmp/sec")
    stdin, stdout, stderr = ssh.exec_command("sudo mv /tmp/sec/lynis/lynis /tmp/sec/lynis/lynis.sh")
    stdin, stdout, stderr = ssh.exec_command(
        "bash /tmp/sec/lynis/lynis.sh audit system --report-file /tmp/sec/report/lynis-report.log 1>&2", get_pty=True)
    stdout.read()
    download_report(sftp,host,logger)


def download_report(sftp,host,logger):
    data_time = str(time.strftime('%Y-%m-%d ', time.localtime(time.time())).strip() + "_" + time.strftime('%H-%M-%S',
                                                                                                          time.localtime(
                                                                                                              time.time())))
    local_dir = base_dir + "/output/%s/%s_system_scan.log" % (host,data_time)
    tool_list = sftp.get("/tmp/sec/report/lynis-report.log", local_dir)
    logger.info("[+]Virus scan report download success and saved in %s.[+]" % local_dir)
    return tool_list
