#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cli import exec_aws_command
from ec2_controller import *


def reclaim_core(instance_info, keypair_name):
    instance_id_list = list()
    volume_id_list = list()
    image_id_list = list()
    snapshot_id_list = list()
    for info in instance_info:
        instance_id = instance_info[info]["instance_id"]
        instance_id_list.append(instance_id)
        volume_id = instance_info[info]["volume_id"]
        volume_id_list.append(volume_id)
        image_id = instance_info[info]["image_id"]
        image_id_list.append(image_id)
        snapshot_id = exec_aws_command(
            ["ec2", "describe-volumes", "--volume-ids", volume_id, "--query", "Volumes[*].SnapshotId"])
        snapshot_id_list.append(snapshot_id)
    reclaim_instance(instance_id_list)
    reclaim_image(image_id_list)
    reclaim_volume(volume_id_list)
    reclaim_snapshots(snapshot_id_list)
    reclaim_key_pair(keypair_name)


def reclaim_instance(instance_list):
    print "[+]Start reclaiming temporary instance...[+]"
    for instance in instance_list:
        exec_aws_command(["ec2", "terminate-instances", "--instance-ids", instance])


def reclaim_volume(volume_list):
    print "[+]Start reclaiming temporary volume...[+]"
    for volume in volume_list:
        exec_aws_command(["ec2", "delete-volume", "--volume-id", volume])


def reclaim_snapshots(snapshot_list):
    print "[+]Start reclaiming temporary snapshots...[+]"
    for snapshot in snapshot_list:
        exec_aws_command(["ec2", "delete-snapshot", "--snapshot-id", snapshot])


def reclaim_image(image_list):
    print "[+]Start reclaiming temporary image...[+]"
    for image in image_list:
        exec_aws_command(["ec2", "deregister-image", "--image-id", image])


def reclaim_key_pair(keypair_name):
    print "[+]Start reclaiming temporary keypair...[+]"
    exec_aws_command(["ec2", "delete-key-pair", "--key-name", keypair_name])


# reclaim_key_pair("tanzhen")
# reclaim_core(1, 1)
